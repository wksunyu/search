(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Created by gladyu on 17/5/18.
 */
module.exports=[
    require('./sources/youku'),
    require('./sources/aiqiyi'),
    require('./sources/lekan'),
    require('./sources/mangguo'),
    require('./sources/acfun'),
    require('./sources/bilibili')
];
//browserify js/search_data.js > search.js
},{"./sources/acfun":3,"./sources/aiqiyi":4,"./sources/bilibili":5,"./sources/lekan":6,"./sources/mangguo":7,"./sources/youku":8}],2:[function(require,module,exports){
/**
 * Created by gladyu on 17/5/11.
 */
var modules = require('./modules');
var showImg = require("./util/stealimg").showImg;
var search = new Vue({
    el: '#search',
    data: {
        msg:"没有找到相应的影片,试试以下站点",
        jumptab: [
            'http://www.soku.com/m/y/video?q=',
            'http://m.iqiyi.com/search.html?key=',
            'http://m.mgtv.com/#/so?k=',
            'http://m.bilibili.com/search.html?keyword=',
            'http://m.le.com/search?wd=',
            'http://m.acfun.cn/search/?query='
        ],
        logos: [
            {
                "name": "优酷",
                "logo": "./images/ic_logo_yk.png"
            },
            {
                "name": "爱奇艺",
                "logo": "./images/ic_logo_aqy.png"
            },
            {
                "name": "芒果TV",
                "logo": "./images/ic_logo_mg.png"
            },
            {
                "name": "哔哩哔哩",
                "logo": "./images/ic_logo_bili.png"
            },
            {
                "name": "乐视网",
                "logo": "./images/ic_logo_letv.png"
            },
            {
                "name": "ACfun",
                "logo": "./images/ic_logo_acfun.png"
            }

        ],
        recommend: [],
        keyword: '',
        form: '',
        scrolltop:''
    },
    created: function () {
        //读取url中的值
        var key = getUrlInfo('key');
        if($('.key').val().length>0){
            $('.key').val(decodeURI(key));
        }else{
            $('.key').attr('placeholder',decodeURI(key));
        }
        if ($.trim(key) != "") {
            $('.windows8').show();
            var i = 0;
            var timer = setInterval(function () {
                i++;
                if (!!window.WKAPI && !!window.WKAPI.httpUtil) {
                    search.initData(key);
                    clearInterval(timer);
                }
                if (i > 10) {
                    clearInterval(timer);
                    $('.windows8').hide();
                    $('.logo-part').show();
                }
            }, 200);
        }else{
            $('.logo-part').show();
        }
    },
    methods: {
        initData: function (key_encode) {
            var self = this;
            self.keyword = key_encode;
            //改变url中搜索关键字
            window.history.pushState(null, null, "?key="+self.keyword);
            self.recommend = [];
            setTimeout(function(){
                $('.windows8').hide();
                $('.logo-part').show();
            },12000);
            modules.forEach(function (module) {
                module.search(key_encode, self.callback);
            });
        },
        callback: function (data, form) {
            var self = this;
            self.form = form;
            if (data.length > 0) {
                $('.windows8').hide();
                self.recommend = self.recommend.concat(data);
                self.recommend.sort(self.orderbyName);
            } else {
                console.log("data is undefined");
            }
        },
        enterPage: function (addr, canSelect) {
            //if (canSelect) {
            //    return;
            //}
            window.location.href = addr;
        },
        showSelect: function (index) {
            this.scrolltop = document.body.scrollTop;
            $('.float-div').show();
            var docu = document.getElementById('btn_' + index).nextElementSibling;
            $(docu).show();
            $('body').css('top', -(document.body.scrollTop) + 'px')
                .addClass('noscroll');
        },
        enterBlock: function (href) {
            window.location.href = href;
        },
        orderbyName: function (a, b) {
            var self = this;
            var result;
            var str = decodeURI(self.keyword).toLowerCase();
            var priorty_a = a.priorty;
            var priorty_b = b.priorty;
            var a = a.title.toLowerCase().replace(/\s+/g, '');
            var b = b.title.toLowerCase().replace(/\s+/g, '');
            //a,b 都以str开始
            if (a.startsWith(str) && b.startsWith(str)) {
                if (a.length < b.length) {
                    result = -1;
                } else if (a.length > b.length) {
                    result = 1;
                } else {
                    if(priorty_a < priorty_b){
                        result = -1;
                    }else if(priorty_a > priorty_b){
                        result = 1;
                    }else{
                        result = 0;
                    }
                }
            } else {
                if (a.startsWith(str)) {
                    result = -1;
                } else if (b.startsWith(str)) {
                    result = 1;
                } else if (a.indexOf(str) > -1 && b.indexOf(str) > -1) {
                    if(a.length != b.length){
                        result = (a.length < b.length) ? -1 : 1;
                    }else{
                        if(priorty_a < priorty_b){
                            result = -1;
                        }else if(priorty_a > priorty_b){
                            result = 1;
                        }else{
                            result = 0;
                        }
                    }
                } else if (a.indexOf(str) > -1) {
                    result = -1;
                } else if (b.indexOf(str) > -1) {
                    result = 1;
                } else {
                    if(priorty_a < priorty_b){
                        result = -1;
                    }else if(priorty_a > priorty_b){
                        result = 1;
                    }else{
                        result = 0;
                    }
                }
            }
            return result;
        },
        hideFloat: function () {
            $('.float-div').hide();
            $('.list-block').hide();
            $('.list-grad').hide();
            $('body').removeClass('noscroll').scrollTop(this.scrolltop);
        },
        jump: function (index) {
            var self = this;
            window.location.href = self.jumptab[index] + self.keyword;
        },
        search: function () {
            this.msg = "没有找到相应的影片,试试以下站点";
            var key = $('.key').val();
            if ($.trim(key) != "") {
                $('.windows8').show();
                $('.logo-part').hide();
                var key_encode = encodeURI(key);
                this.initData(key_encode);
                $('.key').blur();
                $('iframe').remove();
            }
        },
        cleartext: function() {
            $('.key').val('');
            $('.close-icon').hide();
        }
    },
    watch: {
        recommend: function (newValue, oldValue) {
            if (newValue.length > 0) {
                showImg(this.form);
                $('.logo-part').show();
                this.msg = "选择以下站点，搜索更多关于“"+ decodeURI(this.keyword) + "”影片结果";
            }
        }
    }
});
function getUrlInfo(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return r[2];
    return '';
}
$('.key').on("change paste keyup", function() {
   if($('.key').val().length > 0){
       $('.close-icon').show();
   }else{
       $('.close-icon').hide();
   }
});
},{"./modules":1,"./util/stealimg":11}],3:[function(require,module,exports){
/**
 * Created by gladyu on 17/5/18.
 */
var meta = {
    "form": "acfun",
    "icon": "./images/ic_acfun_2.png",
    "priorty": 6
}
var httpRequest = require("../util/httprequest").httpRequest;
var mListData = require("../util/listhandle").list;
var srcType = require("../util/listhandle").srcType;
var maxRecommedNum = require("../util/listhandle").maxRecommedNum;
exports.search = function (keyword, callback) {
    var header = {deviceType: 2};
    httpRequest("http://search.app.acfun.cn/navigate?q=" + keyword + "&sortField=score&aiCount=1&spCount=1&greenCount=0&listCount=20&userCount=0"
        , "get", "", "", function cb(data) {
            var data = data.data.page;
            var limitNum = 0;
            var list = [];
            if (data.ai && data.ai.length > 0) {
                for (index in data.ai) {
                    result = data.ai[index] ? data.ai[index] : undefined;
                    if (result != undefined && result.visibleLevel != -1 && maxRecommedNum > limitNum) {
                        try {
                            var listdata = mListData.createNew();
                            limitNum++;
                            listdata.pic = result.titleImg;
                            listdata.type = "动漫";
                            listdata.times = result.year;
                            //listdata.author = "";
                            listdata.title = result.title;
                            listdata.icon = meta.icon;
                            listdata.form = meta.form;
                            listdata.priorty = meta.priorty;
                            listdata.href = "http://m.acfun.cn/v/?ab=" + result.id;
                            list.push(listdata);
                        } catch (err) {
                            //
                        }
                        listdata = null;
                    }
                }
            } else {
                for (index in data.list) {
                    result = data.list[index] ? data.list[index] : undefined;
                    if (result != undefined && maxRecommedNum > limitNum) {
                        try {
                            limitNum++;
                            var listdata = mListData.createNew();
                            listdata.pic = result.titleImg;
                            //listdata.type = "";
                            listdata.times = parseInt((result.releaseDate / (1000 * 3600 * 24 * 365)).toFixed(0)) + 1970;
                            //listdata.author = "";
                            listdata.title = result.title;
                            listdata.icon = meta.icon;
                            listdata.form = meta.form;
                            listdata.priorty = meta.priorty;
                            listdata.href = "http://m.acfun.cn/v/?" + result.contentId;
                            list.push(listdata);
                        } catch (err) {

                        }
                        listdata = null;
                    }
                }
            }
            callback(list,meta.form);
        }
    );
};
},{"../util/httprequest":9,"../util/listhandle":10}],4:[function(require,module,exports){
/**
 * Created by gladyu on 17/5/18.
 */
var meta = {
    "form": "iqiyi",
    "icon": "./images/ic_iqiyi_2.png",
    "priorty": 2
}
var httpRequest = require("../util/httprequest").httpRequest;
var mListData = require("../util/listhandle").list;
var srcType = require("../util/listhandle").srcType;
var maxRecommedNum = require("../util/listhandle").maxRecommedNum;
var  checkThisUrl = require("../util/listhandle").checkThisUrl;
exports.search = function (keyword, callback) {
    httpRequest("http://search.video.iqiyi.com/o?if=html5&pageNum=1&pageSize=" + maxRecommedNum + "&key=" + keyword,
        "get", "", "", function cb(data) {
            var data = data.data;
            var limitNum = 0;
            var list = [];
            for (index in data.docinfos) {
                result = data.docinfos[index] ? data.docinfos[index] : undefined;
                if (result != undefined && result.pos === 1 && result.albumDocInfo.siteId == "iqiyi" && maxRecommedNum > 0) {
                    try{
                        limitNum++;
                        var listdata = mListData.createNew();
                        listdata.pic = result.albumDocInfo.albumVImage;
                        listdata.type = result.albumDocInfo.channel.split(',')[0];
                        listdata.times = result.albumDocInfo.releaseDate;
                        listdata.author = result.albumDocInfo.star;
                        listdata.title = result.albumDocInfo.albumTitle;
                        listdata.icon = meta.icon;
                        listdata.form = meta.form;
                        listdata.priorty = meta.priorty;
                        listdata.href = result.albumDocInfo.albumLink;
                        if (result.albumDocInfo.videoinfos.length > 1 && !srcType("电影", listdata.type)) {
                            listdata.canSelect = true;
                            if (srcType("综艺", listdata.type)) {
                                listdata.showType = 'list';
                            }
                            //分集信息收集
                            for (i in result.albumDocInfo.videoinfos) {
                                if (checkThisUrl(result.albumDocInfo.videoinfos[i].itemLink)) {
                                    listdata.serises.push(
                                        {
                                            title: result.albumDocInfo.videoinfos[i].itemTitle,
                                            href: result.albumDocInfo.videoinfos[i].itemLink
                                        }
                                    )
                                } else {
                                    continue;
                                }
                            }
                        }
                        listdata.href = listdata.href.length > 5? listdata.href : result.albumDocInfo.videoinfos[0].itemLink;
                        if(checkThisUrl(listdata.href)){
                            list.push(listdata);
                        }else{
                            limitNum--;
                            continue;
                        }
                    }catch (err){
                        //
                    }
                    listdata = null;
                }
            }
            callback(list,meta.form);
        });

};
},{"../util/httprequest":9,"../util/listhandle":10}],5:[function(require,module,exports){
/**
 * Created by gladyu on 17/5/18.
 */
var meta = {
    "form": "bilibili",
    "icon": "./images/ic_bili_2.png",
    "priorty": 4
}
var httpRequest = require("../util/httprequest").httpRequest;
var mListData = require("../util/listhandle").list;
var srcType = require("../util/listhandle").srcType;
var maxRecommedNum = require("../util/listhandle").maxRecommedNum;
exports.search = function (keyword, callback) {
    httpRequest("https://app.bilibili.com/x/v2/search?keyword=" + keyword + "&pn=1&ps=20",
        "get", "", "", function cb(data) {
            var data = data.data.items;
            var limitNum = 0;
            var list = [];
            for (index in data.archive) {
                result = data.archive[index] ? data.archive[index] : undefined;
                var duration = parseInt(result.duration.split(':')[0]);
                if (result != undefined && maxRecommedNum > limitNum && duration > 20) {
                    try {
                        var listdata = mListData.createNew();
                        limitNum++;
                        listdata.pic = result.cover;
                        //listdata.type = "";
                        //listdata.times = "";
                        listdata.author = result.author;
                        listdata.title = result.title;
                        listdata.icon = meta.icon;
                        listdata.form = meta.form;
                        listdata.priorty = meta.priorty;
                        listdata.href = "http://m.bilibili.com/video/" + result.goto + result.param + ".html";
                        list.push(listdata);
                    }catch (err){
                        //
                    }
                    listdata = null;
                }
            }
            callback(list,meta.form);
        });

};
},{"../util/httprequest":9,"../util/listhandle":10}],6:[function(require,module,exports){
/**
 * Created by gladyu on 17/5/18.
 */
/**
 * Created by gladyu on 17/5/18.
 */
var meta = {
    "form": "lekan",
    "icon": "./images/ic_letv_2.png",
    "priorty": 5
}
var httpRequest = require("../util/httprequest").httpRequest;
var mListData = require("../util/listhandle").list;
var srcType = require("../util/listhandle").srcType;
var maxRecommedNum = require("../util/listhandle").maxRecommedNum;
var  checkThisUrl = require("../util/listhandle").checkThisUrl;
exports.search = function (keyword, callback) {
    httpRequest("http://search.lekan.letv.com/leso/search.so?splatid=1003&ph=420001,420003,-131&mix=3&stat=album,video&wd=" + keyword,
        "get", "", "", function cb(data) {
            var data = data.data;
            var limitNum = 0;
            var list = [];
            for (index in data.album_list) {
                result = data.album_list[index] ? data.album_list[index] : undefined;
                if (result != undefined && result.sub_src == "letv" && maxRecommedNum > limitNum) {
                    try{
                        var listdata = mListData.createNew();
                        limitNum++;
                        listdata.pic = result.poster[18].url != undefined ? result.poster[18].url: result.poster[0].url;
                        listdata.type = result.category.name;
                        listdata.times = parseInt((result.release_date / (1000 * 3600 * 24 * 365)).toFixed(0)) + 1970;
                        for (i in result.starring) {
                            listdata.author = result.starring[i].name + ",";
                        }
                        listdata.title = result.name.replace(/[\u0001-\u0032]/g, '');
                        listdata.icon = meta.icon;
                        listdata.form = meta.form;
                        listdata.href = result.url;
                        listdata.priorty = meta.priorty;
                        if (result.video_list.length > 1 && !srcType("电影", listdata.type)) {
                            listdata.canSelect = true;
                            if (srcType("综艺", listdata.type)) {
                                listdata.showType = 'list';
                            }
                            for (i in result.video_list) {
                                if(checkThisUrl(result.video_list[i].url)){
                                    listdata.serises.push(
                                        {
                                            title: result.video_list[i].name,
                                            href: result.video_list[i].url
                                        }
                                    )
                                }else{
                                    continue;
                                }
                            }
                        }
                        listdata.href = listdata.href.length > 5? listdata.href : result.video_list[0].url;
                        if(checkThisUrl(listdata.href)){
                            list.push(listdata);
                        }else{
                            limitNum--;
                            continue;
                        }
                    }catch(err){
                        //
                    }
                    listdata = null;
                }
            }
            callback(list,meta.form);
        });
};
},{"../util/httprequest":9,"../util/listhandle":10}],7:[function(require,module,exports){
/**
 * Created by gladyu on 17/5/18.
 */
var meta = {
    "form": "mangguo",
    "icon": "./images/ic_mangguo_2.png",
    "priorty": 3
}
var httpRequest = require("../util/httprequest").httpRequest;
var mListData = require("../util/listhandle").list;
var srcType = require("../util/listhandle").srcType;
var maxRecommedNum = require("../util/listhandle").maxRecommedNum;
var  checkThisUrl = require("../util/listhandle").checkThisUrl;
exports.search = function (keyword, callback) {
    httpRequest("http://mobile.api.hunantv.com/v7/search/getResult?name=" + keyword,
        "get", "", "", function cb(data) {
            var data = data.data;
            var limitNum = 0;
            var list = [];
            for (index in data.hitDocs) {
                result = data.hitDocs[index] ? data.hitDocs[index] : undefined;
                if (result != undefined && result.jumpType == "videoPlayer" && maxRecommedNum > limitNum) {
                    try{
                        var listdata = mListData.createNew();
                        limitNum++;
                        listdata.pic = result.image;
                        listdata.type = $.trim(result.videoType.split('/')[0]);
                        listdata.times = result.year;
                        listdata.author = result.desc;
                        listdata.title = result.name;
                        listdata.icon = meta.icon;
                        listdata.form = meta.form;
                        listdata.href = result.webUrl;
                        listdata.priorty = meta.priorty;
                        if (result.videos.length > 0 && listdata.type && !srcType("电影", listdata.type)) {
                            listdata.canSelect = true;
                            if (srcType("综艺", listdata.type)) {
                                listdata.showType = 'list';
                            }
                            for (i in result.videos[0].list) {
                                if(checkThisUrl(result.videos[0].list[i].webUrl)){
                                    listdata.serises.push(
                                        {
                                            title: result.videos[0].list[i].name,
                                            href: result.videos[0].list[i].webUrl
                                        }
                                    )
                                }else{
                                    continue;
                                }

                            }
                        }
                        listdata.href = listdata.href.length > 5? listdata.href : result.videos[0].list[0].webUrl;
                        if(checkThisUrl(listdata.href)){
                            list.push(listdata);
                        }else{
                            limitNum--;
                            continue;
                        }
                    }catch (err){
                        //
                    }
                    listdata = null;
                }
            }
            callback(list,meta.form);
        });
};
},{"../util/httprequest":9,"../util/listhandle":10}],8:[function(require,module,exports){
/**
 * Created by gladyu on 17/5/18.
 */
var meta = {
    "form": "youku",
    "icon": "./images/ic_youku_2.png",
    "priorty": 1
}
var httpRequest = require("../util/httprequest").httpRequest;
var mListData = require("../util/listhandle").list;
var srcType = require("../util/listhandle").srcType;
var maxRecommedNum = require("../util/listhandle").maxRecommedNum;
exports.search = function (keyword, callback) {
    httpRequest("http://api.appsdk.soku.com/i/s?keyword=" + keyword + "&utdid=WRbFZW%2B7FAkDAI5M0PtRIBVC",
        "get", "", "", function cb(data) {
            var limitNum = 0;
            var list = [];
            for (index in data.results) {
                result = data.results[index] ? data.results[index] : undefined;
                if (result != undefined && result.is_youku === 1 && maxRecommedNum > limitNum) {
                    try {
                        var listdata = mListData.createNew();
                        limitNum++;
                        listdata.pic = result.vthumburl;
                        listdata.type = result.cats;
                        listdata.times = result.release_date;
                        listdata.author = result.notice;
                        listdata.title = result.title;
                        listdata.icon = meta.icon;
                        listdata.form = meta.form;
                        listdata.href = "http://m.youku.com/video/id_" + result.showid + ".html";
                        listdata.priorty = meta.priorty;
                        if (result.serises.length > 1 && !srcType("电影", listdata.type)) {
                            listdata.canSelect = true;
                            if (srcType("综艺", listdata.type)) {
                                listdata.showType = 'list';
                            }
                            //分集信息收集
                            for (i in result.serises) {
                                listdata.serises.push(
                                    {
                                        href: "http://m.youku.com/video/id_" + result.serises[i].videoid + ".html",
                                        title: result.serises[i].title
                                    }
                                )
                            }
                        }
                        if(checkThisUrl(listdata.href)){
                            list.push(listdata);
                        }else{
                            limitNum--;
                            continue;
                        }
                    } catch (err) {

                    }
                    listdata = null;
                }
            }
            callback(list, meta.form);
        });

};
},{"../util/httprequest":9,"../util/listhandle":10}],9:[function(require,module,exports){
/**
 * Created by gladyu on 17/5/18.
 */

var callbackPrefix = 'sunyucallback';
var callbackCount  = 0;
var timeoutDelay   = 10000;

function callbackHandle () {
    return callbackPrefix + callbackCount++
}
function dataDecode(data) {
    var resultData = "";
    if (data) {
        resultData = JSON.parse(Base64.decode(data));
    }
    return resultData;
}
exports.httpRequest = function (url, method, params, header, callback) {
    var _callbackHandle = callbackHandle();
    window[_callbackHandle] = function (data) {
        clearTimeout(timeoutTimer);
        window[_callbackHandle] = function () {};
            data = dataDecode(data);
            callback(data);
        _callbackHandle = null;

    };
    var timeoutTimer = setTimeout(function () {
        window[_callbackHandle](-1);
    }, timeoutDelay);
    if (!!WKAPI) {
        WKAPI.httpUtil(url, method, params, header, _callbackHandle);
    }
};
},{}],10:[function(require,module,exports){
/**
 * Created by gladyu on 17/5/18.
 */

exports.list = {
    createNew: function () {
        var listdata = {};
        listdata.pic = '';
        listdata.type = '影视';
        listdata.times = "未知";
        listdata.author = "未知";
        listdata.title = "";
        listdata.href = "";
        listdata.form = "";
        listdata.icon = "";
        listdata.serises = [];
        listdata.showType = "block";
        listdata.priorty = 1;
        return listdata;
    }
};
exports.srcType = function (baseType, formType) {
    var isMeta = false;
    if (formType.indexOf(baseType) > -1) {
        isMeta = true;
    }
    return isMeta;
};
exports.maxRecommedNum = 3;
exports.checkThisUrl = function(url){
    var weigh = false;
    if(url!='' && url.length >10){
        weigh = true;
    }
    return weigh;
};
},{}],11:[function(require,module,exports){
/**
 * Created by gladyu on 17/5/23.
 */
exports.showImg = function(form){
        window.form = form;
        window.ifrimg_html = '<scr'+'ipt>setTimeout(function () {parent.$("img[data-dao][data-form=\'' + window.form +'\']").attr("src", function () {return parent.$(this).data("dao")});parent.$("img[data-dao][data-form=\'' + window.form +'\']").each(function () {if (this.clientWidth > this.parentElement.clientWidth) {var moveX = (this.parentElement.clientWidth - this.clientWidth) / 2;console.log(moveX);this.style.webkitTransform = "translateX(" + moveX + "px)";this.style.MozTransform = "translateX(" + moveX + "px)";this.style.msTransform = "translateX(" + moveX + "px)";this.style.OTransform = "translateX(" + moveX + "px)";this.style.transform = "translateX(" + moveX + "px)";}});}, 789);</scr'+'ipt>';
        $("img[data-dao][data-form='" + form +"']").each(function () {
                window.ifrimg_html += '<img src="' + $(this).data('dao') + '" class="list-pic"/>';
        });
        $('body').append('<iframe src="javascript:parent.ifrimg_html;" style="display:none;"></iframe>');
        window.ifrimg_html = null;
        window.form = null;
};
},{}]},{},[2]);
