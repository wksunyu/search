/**
 * Created by gladyu on 17/5/18.
 */
module.exports=[
    require('./sources/youku'),
    require('./sources/aiqiyi'),
    require('./sources/lekan'),
    require('./sources/mangguo'),
    require('./sources/acfun'),
    require('./sources/bilibili')
];
//browserify js/search_data.js > search.js