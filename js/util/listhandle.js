/**
 * Created by gladyu on 17/5/18.
 */

exports.list = {
    createNew: function () {
        var listdata = {};
        listdata.pic = '';
        listdata.type = '影视';
        listdata.times = "未知";
        listdata.author = "未知";
        listdata.title = "";
        listdata.href = "";
        listdata.form = "";
        listdata.icon = "";
        listdata.serises = [];
        listdata.showType = "block";
        listdata.priorty = 1;
        return listdata;
    }
};
exports.srcType = function (baseType, formType) {
    var isMeta = false;
    if (formType.indexOf(baseType) > -1) {
        isMeta = true;
    }
    return isMeta;
};
exports.maxRecommedNum = 3;
exports.checkThisUrl = function(url){
    var weigh = false;
    if(url!='' && url.length >10){
        weigh = true;
    }
    return weigh;
};