/**
 * Created by gladyu on 17/5/18.
 */

var callbackPrefix = 'sunyucallback';
var callbackCount  = 0;
var timeoutDelay   = 10000;

function callbackHandle () {
    return callbackPrefix + callbackCount++
}
function dataDecode(data) {
    var resultData = "";
    if (data) {
        resultData = JSON.parse(Base64.decode(data));
    }
    return resultData;
}
exports.httpRequest = function (url, method, params, header, callback) {
    var _callbackHandle = callbackHandle();
    window[_callbackHandle] = function (data) {
        clearTimeout(timeoutTimer);
        window[_callbackHandle] = function () {};
            data = dataDecode(data);
            callback(data);
        _callbackHandle = null;

    };
    var timeoutTimer = setTimeout(function () {
        window[_callbackHandle](-1);
    }, timeoutDelay);
    if (!!WKAPI) {
        WKAPI.httpUtil(url, method, params, header, _callbackHandle);
    }
};