/**
 * Created by gladyu on 17/5/11.
 */
var modules = require('./modules');
var showImg = require("./util/stealimg").showImg;
var search = new Vue({
    el: '#search',
    data: {
        msg:"没有找到相应的影片,试试以下站点",
        jumptab: [
            'http://www.soku.com/m/y/video?q=',
            'http://m.iqiyi.com/search.html?key=',
            'http://m.mgtv.com/#/so?k=',
            'http://m.bilibili.com/search.html?keyword=',
            'http://m.le.com/search?wd=',
            'http://m.acfun.cn/search/?query='
        ],
        logos: [
            {
                "name": "优酷",
                "logo": "./images/ic_logo_yk.png"
            },
            {
                "name": "爱奇艺",
                "logo": "./images/ic_logo_aqy.png"
            },
            {
                "name": "芒果TV",
                "logo": "./images/ic_logo_mg.png"
            },
            {
                "name": "哔哩哔哩",
                "logo": "./images/ic_logo_bili.png"
            },
            {
                "name": "乐视网",
                "logo": "./images/ic_logo_letv.png"
            },
            {
                "name": "ACfun",
                "logo": "./images/ic_logo_acfun.png"
            }

        ],
        recommend: [],
        keyword: '',
        form: '',
        scrolltop:''
    },
    created: function () {
        //读取url中的值
        var key = getUrlInfo('key');
        if($('.key').val().length>0){
            $('.key').val(decodeURI(key));
        }else{
            $('.key').attr('placeholder',decodeURI(key));
        }
        if ($.trim(key) != "") {
            $('.windows8').show();
            var i = 0;
            var timer = setInterval(function () {
                i++;
                if (!!window.WKAPI && !!window.WKAPI.httpUtil) {
                    search.initData(key);
                    clearInterval(timer);
                }
                if (i > 10) {
                    clearInterval(timer);
                    $('.windows8').hide();
                    $('.logo-part').show();
                }
            }, 200);
        }else{
            $('.logo-part').show();
        }
    },
    methods: {
        initData: function (key_encode) {
            var self = this;
            self.keyword = key_encode;
            //改变url中搜索关键字
            window.history.pushState(null, null, "?key="+self.keyword);
            self.recommend = [];
            setTimeout(function(){
                $('.windows8').hide();
                $('.logo-part').show();
            },12000);
            modules.forEach(function (module) {
                module.search(key_encode, self.callback);
            });
        },
        callback: function (data, form) {
            var self = this;
            self.form = form;
            if (data.length > 0) {
                $('.windows8').hide();
                self.recommend = self.recommend.concat(data);
                self.recommend.sort(self.orderbyName);
            } else {
                console.log("data is undefined");
            }
        },
        enterPage: function (addr, canSelect) {
            //if (canSelect) {
            //    return;
            //}
            window.location.href = addr;
        },
        showSelect: function (index) {
            this.scrolltop = document.body.scrollTop;
            $('.float-div').show();
            var docu = document.getElementById('btn_' + index).nextElementSibling;
            $(docu).show();
            $('body').css('top', -(document.body.scrollTop) + 'px')
                .addClass('noscroll');
        },
        enterBlock: function (href) {
            window.location.href = href;
        },
        orderbyName: function (a, b) {
            var self = this;
            var result;
            var str = decodeURI(self.keyword).toLowerCase();
            var priorty_a = a.priorty;
            var priorty_b = b.priorty;
            var a = a.title.toLowerCase().replace(/\s+/g, '');
            var b = b.title.toLowerCase().replace(/\s+/g, '');
            //a,b 都以str开始
            if (a.startsWith(str) && b.startsWith(str)) {
                if (a.length < b.length) {
                    result = -1;
                } else if (a.length > b.length) {
                    result = 1;
                } else {
                    if(priorty_a < priorty_b){
                        result = -1;
                    }else if(priorty_a > priorty_b){
                        result = 1;
                    }else{
                        result = 0;
                    }
                }
            } else {
                if (a.startsWith(str)) {
                    result = -1;
                } else if (b.startsWith(str)) {
                    result = 1;
                } else if (a.indexOf(str) > -1 && b.indexOf(str) > -1) {
                    if(a.length != b.length){
                        result = (a.length < b.length) ? -1 : 1;
                    }else{
                        if(priorty_a < priorty_b){
                            result = -1;
                        }else if(priorty_a > priorty_b){
                            result = 1;
                        }else{
                            result = 0;
                        }
                    }
                } else if (a.indexOf(str) > -1) {
                    result = -1;
                } else if (b.indexOf(str) > -1) {
                    result = 1;
                } else {
                    if(priorty_a < priorty_b){
                        result = -1;
                    }else if(priorty_a > priorty_b){
                        result = 1;
                    }else{
                        result = 0;
                    }
                }
            }
            return result;
        },
        hideFloat: function () {
            $('.float-div').hide();
            $('.list-block').hide();
            $('.list-grad').hide();
            $('body').removeClass('noscroll').scrollTop(this.scrolltop);
        },
        jump: function (index) {
            var self = this;
            window.location.href = self.jumptab[index] + self.keyword;
        },
        search: function () {
            this.msg = "没有找到相应的影片,试试以下站点";
            var key = $('.key').val();
            if ($.trim(key) != "") {
                $('.windows8').show();
                $('.logo-part').hide();
                var key_encode = encodeURI(key);
                this.initData(key_encode);
                $('.key').blur();
                $('iframe').remove();
            }
        },
        cleartext: function() {
            $('.key').val('');
            $('.close-icon').hide();
        }
    },
    watch: {
        recommend: function (newValue, oldValue) {
            if (newValue.length > 0) {
                showImg(this.form);
                $('.logo-part').show();
                this.msg = "选择以下站点，搜索更多关于“"+ decodeURI(this.keyword) + "”影片结果";
            }
        }
    }
});
function getUrlInfo(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return r[2];
    return '';
}
$('.key').on("change paste keyup", function() {
   if($('.key').val().length > 0){
       $('.close-icon').show();
   }else{
       $('.close-icon').hide();
   }
});