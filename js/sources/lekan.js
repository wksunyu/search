/**
 * Created by gladyu on 17/5/18.
 */
/**
 * Created by gladyu on 17/5/18.
 */
var meta = {
    "form": "lekan",
    "icon": "./images/ic_letv_2.png",
    "priorty": 5
}
var httpRequest = require("../util/httprequest").httpRequest;
var mListData = require("../util/listhandle").list;
var srcType = require("../util/listhandle").srcType;
var maxRecommedNum = require("../util/listhandle").maxRecommedNum;
var  checkThisUrl = require("../util/listhandle").checkThisUrl;
exports.search = function (keyword, callback) {
    httpRequest("http://search.lekan.letv.com/leso/search.so?splatid=1003&ph=420001,420003,-131&mix=3&stat=album,video&wd=" + keyword,
        "get", "", "", function cb(data) {
            var data = data.data;
            var limitNum = 0;
            var list = [];
            for (index in data.album_list) {
                result = data.album_list[index] ? data.album_list[index] : undefined;
                if (result != undefined && result.sub_src == "letv" && maxRecommedNum > limitNum) {
                    try{
                        var listdata = mListData.createNew();
                        limitNum++;
                        listdata.pic = result.poster[18].url != undefined ? result.poster[18].url: result.poster[0].url;
                        listdata.type = result.category.name;
                        listdata.times = parseInt((result.release_date / (1000 * 3600 * 24 * 365)).toFixed(0)) + 1970;
                        for (i in result.starring) {
                            listdata.author = result.starring[i].name + ",";
                        }
                        listdata.title = result.name.replace(/[\u0001-\u0032]/g, '');
                        listdata.icon = meta.icon;
                        listdata.form = meta.form;
                        listdata.href = result.url;
                        listdata.priorty = meta.priorty;
                        if (result.video_list.length > 1 && !srcType("电影", listdata.type)) {
                            listdata.canSelect = true;
                            if (srcType("综艺", listdata.type)) {
                                listdata.showType = 'list';
                            }
                            for (i in result.video_list) {
                                if(checkThisUrl(result.video_list[i].url)){
                                    listdata.serises.push(
                                        {
                                            title: result.video_list[i].name,
                                            href: result.video_list[i].url
                                        }
                                    )
                                }else{
                                    continue;
                                }
                            }
                        }
                        listdata.href = listdata.href.length > 5? listdata.href : result.video_list[0].url;
                        if(checkThisUrl(listdata.href)){
                            list.push(listdata);
                        }else{
                            limitNum--;
                            continue;
                        }
                    }catch(err){
                        //
                    }
                    listdata = null;
                }
            }
            callback(list,meta.form);
        });
};