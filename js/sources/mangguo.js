/**
 * Created by gladyu on 17/5/18.
 */
var meta = {
    "form": "mangguo",
    "icon": "./images/ic_mangguo_2.png",
    "priorty": 3
}
var httpRequest = require("../util/httprequest").httpRequest;
var mListData = require("../util/listhandle").list;
var srcType = require("../util/listhandle").srcType;
var maxRecommedNum = require("../util/listhandle").maxRecommedNum;
var  checkThisUrl = require("../util/listhandle").checkThisUrl;
exports.search = function (keyword, callback) {
    httpRequest("http://mobile.api.hunantv.com/v7/search/getResult?name=" + keyword,
        "get", "", "", function cb(data) {
            var data = data.data;
            var limitNum = 0;
            var list = [];
            for (index in data.hitDocs) {
                result = data.hitDocs[index] ? data.hitDocs[index] : undefined;
                if (result != undefined && result.jumpType == "videoPlayer" && maxRecommedNum > limitNum) {
                    try{
                        var listdata = mListData.createNew();
                        limitNum++;
                        listdata.pic = result.image;
                        listdata.type = $.trim(result.videoType.split('/')[0]);
                        listdata.times = result.year;
                        listdata.author = result.desc;
                        listdata.title = result.name;
                        listdata.icon = meta.icon;
                        listdata.form = meta.form;
                        listdata.href = result.webUrl;
                        listdata.priorty = meta.priorty;
                        if (result.videos.length > 0 && listdata.type && !srcType("电影", listdata.type)) {
                            listdata.canSelect = true;
                            if (srcType("综艺", listdata.type)) {
                                listdata.showType = 'list';
                            }
                            for (i in result.videos[0].list) {
                                if(checkThisUrl(result.videos[0].list[i].webUrl)){
                                    listdata.serises.push(
                                        {
                                            title: result.videos[0].list[i].name,
                                            href: result.videos[0].list[i].webUrl
                                        }
                                    )
                                }else{
                                    continue;
                                }

                            }
                        }
                        listdata.href = listdata.href.length > 5? listdata.href : result.videos[0].list[0].webUrl;
                        if(checkThisUrl(listdata.href)){
                            list.push(listdata);
                        }else{
                            limitNum--;
                            continue;
                        }
                    }catch (err){
                        //
                    }
                    listdata = null;
                }
            }
            callback(list,meta.form);
        });
};