/**
 * Created by gladyu on 17/5/18.
 */
var meta = {
    "form": "youku",
    "icon": "./images/ic_youku_2.png",
    "priorty": 1
}
var httpRequest = require("../util/httprequest").httpRequest;
var mListData = require("../util/listhandle").list;
var srcType = require("../util/listhandle").srcType;
var maxRecommedNum = require("../util/listhandle").maxRecommedNum;
exports.search = function (keyword, callback) {
    httpRequest("http://api.appsdk.soku.com/i/s?keyword=" + keyword + "&utdid=WRbFZW%2B7FAkDAI5M0PtRIBVC",
        "get", "", "", function cb(data) {
            var limitNum = 0;
            var list = [];
            for (index in data.results) {
                result = data.results[index] ? data.results[index] : undefined;
                if (result != undefined && result.is_youku === 1 && maxRecommedNum > limitNum) {
                    try {
                        var listdata = mListData.createNew();
                        limitNum++;
                        listdata.pic = result.vthumburl;
                        listdata.type = result.cats;
                        listdata.times = result.release_date;
                        listdata.author = result.notice;
                        listdata.title = result.title;
                        listdata.icon = meta.icon;
                        listdata.form = meta.form;
                        listdata.href = "http://m.youku.com/video/id_" + result.showid + ".html";
                        listdata.priorty = meta.priorty;
                        if (result.serises.length > 1 && !srcType("电影", listdata.type)) {
                            listdata.canSelect = true;
                            if (srcType("综艺", listdata.type)) {
                                listdata.showType = 'list';
                            }
                            //分集信息收集
                            for (i in result.serises) {
                                listdata.serises.push(
                                    {
                                        href: "http://m.youku.com/video/id_" + result.serises[i].videoid + ".html",
                                        title: result.serises[i].title
                                    }
                                )
                            }
                        }
                        if(checkThisUrl(listdata.href)){
                            list.push(listdata);
                        }else{
                            limitNum--;
                            continue;
                        }
                    } catch (err) {

                    }
                    listdata = null;
                }
            }
            callback(list, meta.form);
        });

};