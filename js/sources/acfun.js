/**
 * Created by gladyu on 17/5/18.
 */
var meta = {
    "form": "acfun",
    "icon": "./images/ic_acfun_2.png",
    "priorty": 6
}
var httpRequest = require("../util/httprequest").httpRequest;
var mListData = require("../util/listhandle").list;
var srcType = require("../util/listhandle").srcType;
var maxRecommedNum = require("../util/listhandle").maxRecommedNum;
exports.search = function (keyword, callback) {
    var header = {deviceType: 2};
    httpRequest("http://search.app.acfun.cn/navigate?q=" + keyword + "&sortField=score&aiCount=1&spCount=1&greenCount=0&listCount=20&userCount=0"
        , "get", "", "", function cb(data) {
            var data = data.data.page;
            var limitNum = 0;
            var list = [];
            if (data.ai && data.ai.length > 0) {
                for (index in data.ai) {
                    result = data.ai[index] ? data.ai[index] : undefined;
                    if (result != undefined && result.visibleLevel != -1 && maxRecommedNum > limitNum) {
                        try {
                            var listdata = mListData.createNew();
                            limitNum++;
                            listdata.pic = result.titleImg;
                            listdata.type = "动漫";
                            listdata.times = result.year;
                            //listdata.author = "";
                            listdata.title = result.title;
                            listdata.icon = meta.icon;
                            listdata.form = meta.form;
                            listdata.priorty = meta.priorty;
                            listdata.href = "http://m.acfun.cn/v/?ab=" + result.id;
                            list.push(listdata);
                        } catch (err) {
                            //
                        }
                        listdata = null;
                    }
                }
            } else {
                for (index in data.list) {
                    result = data.list[index] ? data.list[index] : undefined;
                    if (result != undefined && maxRecommedNum > limitNum) {
                        try {
                            limitNum++;
                            var listdata = mListData.createNew();
                            listdata.pic = result.titleImg;
                            //listdata.type = "";
                            listdata.times = parseInt((result.releaseDate / (1000 * 3600 * 24 * 365)).toFixed(0)) + 1970;
                            //listdata.author = "";
                            listdata.title = result.title;
                            listdata.icon = meta.icon;
                            listdata.form = meta.form;
                            listdata.priorty = meta.priorty;
                            listdata.href = "http://m.acfun.cn/v/?" + result.contentId;
                            list.push(listdata);
                        } catch (err) {

                        }
                        listdata = null;
                    }
                }
            }
            callback(list,meta.form);
        }
    );
};