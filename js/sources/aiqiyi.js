/**
 * Created by gladyu on 17/5/18.
 */
var meta = {
    "form": "iqiyi",
    "icon": "./images/ic_iqiyi_2.png",
    "priorty": 2
}
var httpRequest = require("../util/httprequest").httpRequest;
var mListData = require("../util/listhandle").list;
var srcType = require("../util/listhandle").srcType;
var maxRecommedNum = require("../util/listhandle").maxRecommedNum;
var  checkThisUrl = require("../util/listhandle").checkThisUrl;
exports.search = function (keyword, callback) {
    httpRequest("http://search.video.iqiyi.com/o?if=html5&pageNum=1&pageSize=" + maxRecommedNum + "&key=" + keyword,
        "get", "", "", function cb(data) {
            var data = data.data;
            var limitNum = 0;
            var list = [];
            for (index in data.docinfos) {
                result = data.docinfos[index] ? data.docinfos[index] : undefined;
                if (result != undefined && result.pos === 1 && result.albumDocInfo.siteId == "iqiyi" && maxRecommedNum > 0) {
                    try{
                        limitNum++;
                        var listdata = mListData.createNew();
                        listdata.pic = result.albumDocInfo.albumVImage;
                        listdata.type = result.albumDocInfo.channel.split(',')[0];
                        listdata.times = result.albumDocInfo.releaseDate;
                        listdata.author = result.albumDocInfo.star;
                        listdata.title = result.albumDocInfo.albumTitle;
                        listdata.icon = meta.icon;
                        listdata.form = meta.form;
                        listdata.priorty = meta.priorty;
                        listdata.href = result.albumDocInfo.albumLink;
                        if (result.albumDocInfo.videoinfos.length > 1 && !srcType("电影", listdata.type)) {
                            listdata.canSelect = true;
                            if (srcType("综艺", listdata.type)) {
                                listdata.showType = 'list';
                            }
                            //分集信息收集
                            for (i in result.albumDocInfo.videoinfos) {
                                if (checkThisUrl(result.albumDocInfo.videoinfos[i].itemLink)) {
                                    listdata.serises.push(
                                        {
                                            title: result.albumDocInfo.videoinfos[i].itemTitle,
                                            href: result.albumDocInfo.videoinfos[i].itemLink
                                        }
                                    )
                                } else {
                                    continue;
                                }
                            }
                        }
                        listdata.href = listdata.href.length > 5? listdata.href : result.albumDocInfo.videoinfos[0].itemLink;
                        if(checkThisUrl(listdata.href)){
                            list.push(listdata);
                        }else{
                            limitNum--;
                            continue;
                        }
                    }catch (err){
                        //
                    }
                    listdata = null;
                }
            }
            callback(list,meta.form);
        });

};