/**
 * Created by gladyu on 17/5/18.
 */
var meta = {
    "form": "bilibili",
    "icon": "./images/ic_bili_2.png",
    "priorty": 4
}
var httpRequest = require("../util/httprequest").httpRequest;
var mListData = require("../util/listhandle").list;
var srcType = require("../util/listhandle").srcType;
var maxRecommedNum = require("../util/listhandle").maxRecommedNum;
exports.search = function (keyword, callback) {
    httpRequest("https://app.bilibili.com/x/v2/search?keyword=" + keyword + "&pn=1&ps=20",
        "get", "", "", function cb(data) {
            var data = data.data.items;
            var limitNum = 0;
            var list = [];
            for (index in data.archive) {
                result = data.archive[index] ? data.archive[index] : undefined;
                var duration = parseInt(result.duration.split(':')[0]);
                if (result != undefined && maxRecommedNum > limitNum && duration > 20) {
                    try {
                        var listdata = mListData.createNew();
                        limitNum++;
                        listdata.pic = result.cover;
                        //listdata.type = "";
                        //listdata.times = "";
                        listdata.author = result.author;
                        listdata.title = result.title;
                        listdata.icon = meta.icon;
                        listdata.form = meta.form;
                        listdata.priorty = meta.priorty;
                        listdata.href = "http://m.bilibili.com/video/" + result.goto + result.param + ".html";
                        list.push(listdata);
                    }catch (err){
                        //
                    }
                    listdata = null;
                }
            }
            callback(list,meta.form);
        });

};